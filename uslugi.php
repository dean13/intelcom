<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>INTEL-COM</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

        <link href="http://fonts.googleapis.com/css?family=Roboto:300&amp;subset=latin-ext" rel="stylesheet">
    </head>
    <body>
        <?php
        // put your code here
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".lista-strony").hover(
                        function () {
                            $(this).css("height", "400px");
                        }, function () {
                    $(".lista-strony").attr("style", "");
                    //$(this).find("p:last").remove();
                }
                );
                $(".lista-strony").hover(
                        function () {
                            $(this).append("<div class='pozycja nowa-pozycja'><p> - tworzenie i budowa stron internetowych\n\
                                            <br> - naprawa i wsparcie dla już istniejących stron\n\
                                            <br> - opieka administracyjna nad stronami internetowymi\n\
                                            <br> - rozbudowa istniejących stron\n\
                                            <br> - strony wizytówki dla firm/osób prywatnych\n\
                                            <br> - programy bazodanowe\n\
                                            <br> - własne serwery, przestrzeń hostingowa</p></div>");
                        }, function () {
                    $(".lista-strony").attr("style", "");
                    $(this).find("p:last").remove();
                }
                );
            });
        </script>
        <div class="header">
            <div class="menu">
                <a href="index.php">Główna</a>
                <a href="uslugi.php">Usługi</a>
                <a href="pogotowie.php">Pogotowie inf.</a>
                <a href="cennik.php">Cennik</a>
            </div>
            <div class="panel"> 
                <h1>INTEL-COM</h1>
                <hr class="redLine_default"></hr>
                <p class="about_factory">Zajmujemy się szeroką gamą usług informatycznych począwszy od projektowania witryn internetowych a nad opieką
                    informatyczną dla firm skończywczy. Cenimy sobie jakość oraz podążamy za nadchodzącymi trendami.</p>
            </div>
        </div>

        <div class="panel-uslugi">
            <div class="panel-naglowek"><h1>Oferujemy kompleksowe usługi informatyczno-serwisowe</h1></div>
            <div class="panel-lista">
                <ul class="lista-uslug">
                    <li class="lista-uslugi lista-strony"><h3>Projektowanie stron internetowych</h3>
                        <div class="pozycja nowa-pozycja">   
                            <p>Zajmujemy się profesionalnym projektowaniem stron internetowych zgodnych ze standardami i dopasowane do
                                zmieniających się trendów rynku. Każda strona jest responsywna przystosowana do wyświetlania na wielu urządzeniach. 
                                Korzystamy z najnowszych i sprawdzonych technologi oraz narzędzi.</p>
                            <img src="images/uslugi.png"/>
                        </div>
                    </li>
                    <li class="lista-uslugi"><h3 class="header-left">Tworzenie apliakcji na urządzenia Android, iOS i Windos Phone</h3>
                        <div class="pozycja-left">   
                            <img src="images/smartphone.png"/>
                            <p>Projektujemy również aplikacje na urządzenia mobilne takie jak Android, iOS i Windows Phone. 
                                Apliakcje mogą składać się na przejrzystą wizytówkę firmy czy rozbudowaną modułowo aplikację zintegrowaną z bazą danych i dostępem do internetu.
                            </p>                           
                        </div>
                    </li>
                    <li class="lista-uslugi"><h3>Serwis sprzętu informatycznego.</h3>
                        <div class="pozycja">   
                            <img src="images/repair.png"/>
                            <p>Serwisowanie sprzętu informatycznego znajdującego się w firmie klienta. W tym:  komputerów, laptopów, notebooków i innego lub prywatnie w domu klienta.
                            </p>                           
                        </div>
                    </li> 
                    <li class="lista-uslugi"><h3 class="header-left">Instalacja i zarządzanie sieciami komputerowymi</h3>
                        <div class="pozycja-left">   
                            <img src="images/wifi.png"/>
                            <p>instalacja i obsługa sieci LAN, WLAN (zakup sprzętu, montaż, serwis, opieka, rozbudowa, doradztwo) konfiguracja (neostrada, orange, netia, ResMan itp).</p>                           
                        </div>
                    </li>
                    <li class="lista-uslugi"><h3>Odzyskiwanie danych</h3>
                        <div class="pozycja">   
                            <img src="images/recovery.png"/>
                            <p>Odzyskujemy dane z uprzednio przygotowanych nośników lub dysków sieciowych wykonanych wcześniej kopi zapasowych.
                            </p>                           
                        </div>
                    </li>
                    <li class="lista-uslugi"><h3 class="header-left">Szkolenia informatyczne pracowników</h3>
                        <div class="pozycja-left">   
                            <img src="https://cdn3.iconfinder.com/data/icons/ballicons-free/128/graph.png"/>
                            <p>Przeprowadzamy szkolenia informatyczne pracowników – szczególnie przydatne przy wdrażaniu nowych systemów informatycznych, 
                                nowego oprogramowania lub nowych rozwiązań informatycznych.</p>                           
                        </div>
                    </li>
                    <li class="lista-uslugi"><h3>Pomagamy w problemach związanych z systemami operacyjnymi</h3>
                        <div class="pozycja">   
                            <img alt="http://www.icons-land.com" src="http://icons.iconarchive.com/icons/icons-land/vista-people/128/Occupations-Technical-Support-Representative-Male-Light-icon.png"/>
                            <p>Przeprowadzamy szkolenia informatyczne pracowników – szczególnie przydatne przy wdrażaniu nowych systemów informatycznych, 
                                nowego oprogramowania lub nowych rozwiązań informatycznych.</p>                           
                        </div>
                    </li>
                    <li class="lista-uslugi"><h3>Grafika komputerowa projekty i reklama</h3>
                        <div class="pozycja">   
                            <img src="https://cdn0.iconfinder.com/data/icons/web-8/106/Pencil-128.png"/>
                            <p>Wykonujemy projekty graficzne dla użytkowników indywidualnych oraz agencji reklamowych m.in 
                                (wizytówki, broszury, ulotki, plakaty itp.)</p>                           
                        </div>
                    </li>
                    <h2>Wystawiam faktury vat</h2>
                </ul>
            </div>
        </div>
    </body>
</html>
