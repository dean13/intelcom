<?php ?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>INTEL-COM</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="test.php">

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

        <link href="http://fonts.googleapis.com/css?family=Roboto:300&amp;subset=latin-ext" rel="stylesheet">
    </head>
    <body>
        <script type="text/javascript">
            $(document).ready(function () {
                $("li").eq(0).hover(
                        function () {
                            $(this).append($("<p>Naprawiamy wszelkiego rodzaju problemy z komputerami, \n\
                    od błędów oprogramowania po usterki fizyczne i odwirusowanie.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(1).hover(
                        function () {
                            $(this).append($("<p>Serwisujemy komputery stacjonarne i laptopy, wymiana części uszkodzonych na nowe.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(2).hover(
                        function () {
                            $(this).append($("<p>Konserwujemy urządzenia drukujące (drukarki laserowe oraz atramentowe) a także komputery stacjonarne i laptopy.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(3).hover(
                        function () {
                            $(this).append($("<p>Wykonujemy kopie bezpieczeństwa systemów, plików oraz dysków.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(4).hover(
                        function () {
                            $(this).append($("<p>Zapewniamy stałą kontrolę przeciw zagrożeniami z sieci \n\
                    (konfigurujemy zaporę oraz instalujemy potrzebne oprogramowanie)</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(5).hover(
                        function () {
                            $(this).append($("<p>Czuwamy nad stałą konserwacją systemów operacyjnych oraz oprogramowania.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(6).hover(
                        function () {
                            $(this).append($("<p>Służymy pomocą w przypadku modernizacji lub zakupu nowego sprzętu i oprogramowania.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(7).hover(
                        function () {
                            $(this).append($("<p>Instalujemy i konfigurujemy nowe oprogramowanie do poprawnego działania.</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
                $("li").eq(8).hover(
                        function () {
                            $(this).append($("<p>Pomagamy użytkownikom w codziennych problemach ze sprzętem komputerowym</p>"));
                        }, function () {
                    $(this).find("p:last").remove();
                }
                );
            });


        </script>

        <div class="header">
            <div class="menu">
                <a href="index.php">Główna</a>
                <a href="uslugi.php">Usługi</a>
                <a href="pogotowie.php">Pogotowie inf.</a>
                <a href="cennik.php">Cennik</a>
            </div>
            <div class="panel"> 
                <h1>INTEL-COM</h1>
                <hr class="redLine_default"></hr>
                <p class="about_factory">Zajmujemy się szeroką gamą usług informatycznych począwszy od projektowania witryn internetowych a nad opieką
                    informatyczną dla firm skończywczy. Cenimy sobie jakość oraz podążamy za nadchodzącymi trendami.</p>
            </div>
        </div>

        <div class="panel-pogotowie">
            <div class="panel-naglowek"><h1>Proponujemy Państwu stałą lub doraźną opiekę informatyczną nad całością Państwa sprzętu komputerowego oraz oprogramowania.</h1></div>
            <div class="panel-lista">
                <ul class="lista-uslug">
                    <li>Usuwanie wszelkich problemów dotyczących komputerów i oprogramowania</li>
                    <li>Serwis i naprawa komputerów</li>
                    <li>Okresowe konserwacje drukarek i komputerów</li> 
                    <li>Wykonywanie kopii bezpieczeństwa</li>
                    <li>Kontrolę antywirusową</li>
                    <li>Okresowe konserwacje systemów operacyjnych oraz oprogramowania</li>
                    <li>Doradztwo w zakresie modernizacji i zakupu nowego sprzętu i oprogramowania</li>
                    <li>Instalację nowego oprogramowania</li>
                    <li>Wspomaganie użytkowników w rozwiązywaniu bieżących problemów związanych z użytkowaniem komputera</li>
                </ul>
            </div>
        </div>
    </body>
</html>
