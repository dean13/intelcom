﻿<?php
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>INTEL-COM</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="test.php">

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

        <style type="text/css" media="all">@import "/style/Przyklad.css";</style>
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>  

        <link href="http://fonts.googleapis.com/css?family=Roboto:300&amp;subset=latin-ext" rel="stylesheet">


    </head>

    <body onload="mapaStart()"> 
        <div class="header">
            <div class="menu">
                <a href="index.php">Główna</a>
                <a href="uslugi.php">Usługi</a>
                <a href="pogotowie.php">Pogotowie inf.</a>
                <a href="cennik.php">Cennik</a>
            </div>
            <div class="panel"> 
                <h1>INTEL-COM</h1>
                <hr class="redLine_default"></hr>
                <p class="about_factory">Zajmujemy się szeroką gamą usług informatycznych począwszy od projektowania witryn internetowych a nad opieką
                    informatyczną dla firm skończywczy. Cenimy sobie jakość oraz podążamy za nadchodzącymi trendami.</p>
            </div>
        </div>
        
        <div class="uslugi">
            <div class="usluga">
                <a href="uslugi.php"><img src="images/uslugi_03.gif"/></a>
            </div>
            <div class="usluga">
                <a href="pogotowie.php"><img src="images/uslugi_04.gif"/></a>
            </div>
        </div>

        <div class="row">
            <div class="panel-table">                
                <div class="large-6 medium-6 columns center fontLato korespond">
                    <img src="images/danedokorespondencji-ico.png" width="60" height="61" alt=""/> 
                    <p class="header_column">Adres korespondencyjny</p>
                    <p class="Adress"><span>Damian Szela</span>
                    <p class="Phone"><span>Tel:</span> +48 534089954</p>
                    <p class="Email"><span>e-mail:</br></span> <a href="mailto:intelcom.rzeszow@gmail.com">intelcom.rzeszow@gmail.com</a></p>
                </div>     
            </div>            
        </div>
        
         

        <script type="text/javascript">
            <!-- 
            

            var mapa; // obiekt globalny
            var pinColor = "FE7569";
            var mapa1;
            var lokalizacja;
            var MY_MAPTYPE_ID = 'custom_style';
            var geocoder = new google.maps.Geocoder();
            function mapaStart()
            {
                var address = "Mickiewicza 38 37-110 Żołynia";
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        lokalizacja = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                        enableMap();
                    } else {
                        $("#googleMap").html('<p class="about_factory">Nie można wyświetlic mapy.<br />Wskazana lokalizacja jest niepoprawna.</p>');
                    }
                });

                // tworzymy mapę satelitarną i centrujemy w okolicy Rzeszowa na poziomie zoom = 10
                var wspolrzedne = new google.maps.LatLng(50.038632, 21.998326);
                var opcjeMapy = {
                    zoom: 15,
                    center: wspolrzedne,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    backgroundColor: '#FFFFFF'
                };
                mapa = new google.maps.Map(document.getElementById("mapka"), opcjeMapy);

                // stworzenie markera
                var punkt = new google.maps.LatLng(50.038632, 21.998326);
                var opcjeMarkera =
                        {
                            position: punkt,
                            map: mapa,
                            icon: 'images/marker.png',
                            title: "Pierwszy marker!"
                        }
                var marker = new google.maps.Marker(opcjeMarkera);

                // Color each letter gray. Change the color when the isColorful property
                // is set to true.
                map.data.setStyle(function (feature) {
                    var color = 'gray';
                    if (feature.getProperty('isColorful')) {
                        color = feature.getProperty('color');
                    }
                    return /** @type {google.maps.Data.StyleOptions} */({
                        fillColor: color,
                        strokeColor: color,
                        strokeWeight: 2
                    });
                });

            }
-->
        </script>   

        <div class="mapa">
            <p class="header_column">Lokalizacja</p>
            <div id="mapka">   
                <!-- tu będzie mapa -->   
            </div>
        </div>

        <div class="row halfSize">
            <div class="large-12 columns">
                <div class="panel-panel">
                    <div id="formularz">
                        <div id="formularz-do-korespondencji-szczegoly" class="row mfp-hide pop">
                            <div class="large-12 columns">
                                <div class="panel-panel">
                                    <div class="row halfSize" id="replace">
                                        <h2 id="header-msg">Formularz kontaktowy</h2> 
                                        <div class="clear">
                                            <form action="formularz.php" method="post">
                                            <div class="large-6 medium-6 columns">
                                                <input name="email" id="email" value="" placeholder="Wpisz swój e-mail..." class="textInput" type="email" required >
                                            </div>
                                            <div class="large-6 medium-6 columns">
                                                <input name="phone" id="phone" value="" placeholder="Wpisz swój nr telefonu..." class="textInput" type="tel" pattern="\d\d\d\-\d\d\d-\d\d\d" title="xxx-xxx-xxx" required>
                                            </div>
                                            <div class="large-12 columns">
                                                <input name="subject" id="subject" value="" placeholder="Podaj temat wiadomości..." class="textInput" type="text" required>                                    	</div>
                                            <div class="large-12 columns">                    
                                                <textarea name="message" id="message" placeholder="Wpisz treść wiadomości..." class="require" rows="24" cols="80"></textarea>                            </div>
                                            <div class="large-12 columns">
                                                <div class="pull-left">
                                                    <div class="right">
                                                        <input name="send" id="send" value="Wyślij" class="submit" type="submit">   
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
       

        <div class="klienci">
            <h2 id="header-msg">Do grona naszych klientów należą:</h2> 
            <div class="klient"><img src="images/hurtownie.png"/><p>Hurtownie</p></div>
            <div class="klient"><img src="images/carservice.png"/><p>Serwis samochodowy</p></div>
            <div class="klient"><img src="images/biznes.png"/><p>Firmy produkcyjne</p></div>
            <div class="klient"><img src="images/shipyard_icon_large.png"/><p>Branża stoczniowa</p></div>
            <div class="klient"><img src="images/logistyka.png"/><p>Firmy spedycyjne</p></div>
            <div class="klient"><img src="images/sklepy.png"/><p>Sklepy</p></div>
            <div class="klient klient-wide"><img src="images/school.png"/><p>Szkoły</p></div>
            <div class="klient"><img src="images/sanepid.png"/><p>Sanepidy</p></div>
            <div class="klient"><img src="images/homeheart.png"/><p>Domy opieki</p></div>
			<div class="klient"><img src="images/danefirmowe-ico.png"/><p>Klient indywidualny</p></div>
        </div>
        </div>
    </body>
</html>
