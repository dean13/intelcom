﻿<?php
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>INTEL-COM</title>

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="test.php">

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

        <style type="text/css" media="all">@import "/style/Przyklad.css";</style>
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>  

        <link href="http://fonts.googleapis.com/css?family=Roboto:300&amp;subset=latin-ext" rel="stylesheet">
    </head>

    <body>
        <div class="header">
            <div class="menu">
                <a href="index.php">Główna</a>
                <a href="uslugi.php">Usługi</a>
                <a href="pogotowie.php">Pogotowie inf.</a>
                <a href="cennik.php">Cennik</a>
            </div>
            <div class="panel"> 
                <h1>INTEL-COM</h1>
                <hr class="redLine_default"></hr>
                <p class="about_factory">Zajmujemy się szeroką gamą usług informatycznych począwszy od projektowania witryn internetowych a nad opieką
                    informatyczną dla firm skończywczy. Cenimy sobie jakość oraz podążamy za nadchodzącymi trendami.</p>
            </div>
        </div>

        <div class="panel-cennik">
            <div class="info-cennik">
                <div class="opis-cennik">
                    <h2>Cennik usług</h2>
                    <img src="http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/256/Devices-computer-icon.png"/>
                    <p class="text-cennik">Koszt usług oscyluje w granicach od 20 - 1500zł zależnie od wykonywanej usługi i jej czasochłonności. Usługi na których widnieje napis "do uzgodnienia" informuje o złożoności danej usługi i uprzednio należy skontaktować się z wykonawcą. </p>
                </div>
            </div>
            <div class="three-columns">
                <div class="col-uslugi col-rodzaj-uslugi">
                    <div class="row-usluga-header">
                        <h2>Usługi</h2>
                    </div>
                    <div class="col-usluga-header">
                        
                    </div>
                    <div class="row-uslugi">
                        <table width="100%">
                            <thead>
                                <td>Nazwa usługi</td>
                                <td>Cena usługi</td>
                            </thead>
                            <tr>
                                <td>Odzysk zdjęć z karty pamięci np. aparatu – zależnie od  stopnia uszkodzenia</td>
                                <td>70-100</td>
                            </tr>
                            <tr>
                                <td>Odzysk danych z dysku twardego (dowolny system) – indywidualnie </td>
                                <td>120-250</td>
                            </tr>
                            <tr>
                                <td>Odzyskiwanie danych z płyt optycznych oraz urządzen przenośnych.</td>
                                <td>30 - 80</td>
                            </tr>
                            <tr>
                                <td>Trwałe kasowanie danych na dysku (zerowanie bezpowrotne odzyskanie danych)</td>
                                <td>70</td>
                            </tr>
                            <tr>
                                <td>Archiwizacja danych – indywidualnie, zależy od sposobu archiwizacji i ilości danych</td>
                                <td>do uzgodnienia</td>
                            </tr>
                            <tr>
                                <td>Wymiana dysku twardego lub nagrywarki – zależnie od dostępu do napędu.</td>
                                <td>60</td>
                            </tr>
                            <tr>
                                <td>Aktualizacja BIOSu płyty głównej (jeżeli jest dostępny nowszy od istniejącego)</td>
                                <td>50-70</td>
                            </tr>
                            <tr>
                                <td>Czyszczenie klawiatury po różnego rodzaju zalaniach i zanieczyszczeniach.</td>
                                <td>40</td>
                            </tr>
                            <tr>
                                <td>Czyszczenie wentylatora (procesora, karty graficznej) i wymiana pasty.</td>
                                <td>40</td>
                            </tr>
                            <tr>
                                <td>Wymiana ułamanych, wyrwanych zawiasów (zaczepów) do laptopów</td>
                                <td>100</td>
                            </tr>
                            <tr>
                                <td>Wymiana klawiatury zakupienie i zamontowanie nie wliczając kosztów klawiatury !</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>Pozostałe prace wykonywane za jedną roboczą godzinę. </td>
                                <td>50</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-serwis col-rodzaj-uslugi">
                    <div class="row-usluga-header">
                        <h2>Usługi serwisowe</h2>
                    </div>
                    <div class="row-uslugi">
                        <table width="100%">
                            <thead>
                                <td>Nazwa usługi</td>
                                <td>Cena usługi</td>
                            </thead>
                            <tr>
                                <td>Koszt pracy serwisanta za 1 godzinę (nie doliczamy kosztów dojazdu)</td>
                                <td>40</td>
                            </tr>
                            <tr>
                                <td>Instalacja sieci bezprzewodowej</td>
                                <td>120-250</td>
                            </tr>
                            <tr>
                                <td>Instalacja i konfiguracja dostępu do internetu np. przez router</td>
                                <td>40</td>
                            </tr>
                            <tr>
                                <td>Przeinstalowanie systemu Windows (95,98,Me,XP,Vista,7)</td>
                                <td>80</td>
                            </tr>
                            <tr>
                                <td>Instalacja aktualizacji do systemu</td>
                                <td>ok 40</td>
                            </tr>
                            <tr>
                                <td>Diagnoza usterki</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>Czyszczenie wnętrza komputera i z zewnątrz</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>Instalacja programu i jego konfiguracja </td>
                                <td>10 - 30</td>
                            </tr>
                            <tr>
                                <td>Instalacja serwera opartego o rodzinę Windows, Linux, Novell, Unix</td>
                                <td>do uzgodnienia</td>
                            </tr>
                            <tr>
                                <td>Zabezpieczenie i przeniesienie danych podczas przeinstalowywania</td>
                                <td>30- 70</td>
                            </tr>
                            <tr>
                                <td>Odwirusowanie komputera i zainstalowanie Antivirusa</td>
                                <td>40-60</td>
                            </tr>
                            <tr>
                                <td>Naprawa systemu Windows, gdy nie chce się uruchomić</td>
                                <td>50-70</td>
                            </tr>
                            <tr>
                                <td>Wymiana podzespołu komputera lub laptopa</td>
                                <td>15</td>
                            </tr>
                            <tr>
                                <td>Wymiana płyty głównej (+ czasami koszty instalacji systemu na nowo)</td>
                                <td>60-80</td>
                            </tr>
                            <tr>
                                <td>Czyszczenie drukarki, głowic</td>
                                <td>40-60</td>
                            </tr>
                            <tr>
                                <td>Prace pozostałe płatne za 1 godzinę</td>
                                <td>40</td>
                            </tr>
                            <tr>
                                <td>Przeinstalowanie systemu</td>
                                <td>80</td>
                            </tr>
                        </table>
                    </div>

                </div>
                <div class="col-projektowanie col-rodzaj-uslugi">
                    <div class="row-usluga-header">
                        <h2>Usługi projektowe</h2>
                    </div>
                    <div class="row-uslugi">
                        <table width="100%">
                            <thead>
                                <td>Nazwa usługi</td>
                                <td>Cena usługi</td>
                            </thead>
                            <tr>
                                <td>Strona internetowa, sklep, serwis – ustalane indywidualnie w zależności od różnorodności </td>
                                <td>450-1500</td>
                            </tr>
                            <tr>
                                <td>Pozycjonowanie strony w stopniu podstawowym lub zaawansowane pozycjonowanie </td>
                                <td>150-300</td>
                            </tr>
                            <tr>
                                <td>Utrzymanie strony internetowej, sklepu, serwisu – opłata za rok z góry</td>
                                <td>60 - 80</td>
                            </tr>
                            <tr>
                                <td>Rejestracja domeny .pl - opłata za rok z góry </td>
                                <td>60 - 80</td>
                            </tr>
                            <tr>
                                <td>Modyfikacja i wsparcie dla już istniejących stron</td>
                                <td>150-420</td>
                            </tr>
                            <tr>
                                <td>opieka administracyjna nad stronami internetowymi (...zł/miesięcznie)</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>rozbudowa istniejących stron</td>
                                <td>od 300</td>
                            </tr>
                            <tr>
                                <td>strony wizytówki dla firm/osób prywatnych</td>
                                <td>ok 250</td>
                            </tr>
                            <tr>
                                <td>Postawienie sklepu internetowego</td>
                                <td>od 700</td>
                            </tr>
                        </table>
                    </div>                
                </div>
            </div>
        </div>
    </body>
</html>